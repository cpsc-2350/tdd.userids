import { UserIdFactory, UserIdFactoryImpl } from './UserIdFactory';
describe('UserIdFactory', () => {
  let mut: UserIdFactory;
  let mock: jest.MockedFunction<(userId: string) => Promise<boolean>>;

  beforeEach(() => {
    mock = jest.fn();
    mock.mockResolvedValue(false);
    mut = new UserIdFactoryImpl(mock);
  });

  describe('collision', () => {
    function getNumPart(id: string): number {
      const match = /\d+$/.exec(id);
      if (!match) {
        throw new Error();
      }
      return +match[0];
    }

    [
      'alidde01',
      'alidde02',
      'alidde10',
      'alidde99',
      'alidd100',
      'alidd999',
    ].forEach(expected => {
      test(expected, async () => {
        expect.assertions(1);
        const numExpected = getNumPart(expected);
        mock.mockImplementation(
          (userId: string): Promise<boolean> => {
            const numActual = getNumPart(userId);
            return Promise.resolve(numActual < numExpected);
          }
        );
        await expect(mut.makeUserId('Alice', 'Liddel')).resolves.toBe(expected);
      });
    });
    test('all used', async () => {
      expect.assertions(1);
      mock.mockResolvedValue(true);
      await expect(mut.makeUserId('Alice', 'Liddel')).rejects.toThrowError(
        'alidd999 in use'
      );
    });
  });
  describe('names', () => {
    [
      ['Alice', 'Liddell', 'alidde01'],
      ['Bob', 'Barker', 'bbarke01'],
      ['', 'Ng', 'ng01'],
      ['Bob', '', 'b01'],
      ['', '', '01'],
      ['Cloe', 'Ten Brinke', 'ctenbr01'],
      ['Dan', 'A-O-C', 'daoc01'],
      ["'", 'A--O', 'ao01'],
    ].forEach(([first, last, expected]) => {
      test(`'${first}' '${last}'`, async () => {
        await expect(mut.makeUserId(first, last)).resolves.toBe(expected);
      });
    });
  });

  /*
  test('no collisions', async () => {
    await expect(mut.makeUserId('Alice', 'Liddell')).resolves.toBe('alidde01');
  });

  test('01 collision', async () => {
    mock.mockResolvedValueOnce(true);
    mock.mockResolvedValueOnce(false);
    await expect(mut.makeUserId('Alice', 'Liddell')).resolves.toBe('alidde02');
  });

  test('98 collision', async () => {
    mock.mockResolvedValueOnce(true);
    mock.mockResolvedValueOnce(false);
    await expect(mut.makeUserId('Alice', 'Liddell')).resolves.toBe('alidde02');
  });
*/
});
