export interface UserIdFactory {
  makeUserId(first: string, last: string): Promise<string>;
}

export class UserIdFactoryImpl implements UserIdFactory {
  constructor(
    private readonly isUserIdInUse: (userId: string) => Promise<boolean>
  ) {}
  async makeUserId(first: string, last: string): Promise<string> {
    let name = first.length > 0 ? first[0] : '';
    name += last;
    name = name.toLowerCase();
    name = name.replace(/[^a-z]/g, ''); // also removes accented characters
    let id = name;
    for (let i = 1; i <= 999; i++) {
      const strI = i < 10 ? `0${i}` : `${i}`;
      id = name.substr(0, 8 - strI.length) + strI;
      if (!(await this.isUserIdInUse(id))) {
        return id;
      }
    }
    throw new Error(`${id} in use`);
  }
}
